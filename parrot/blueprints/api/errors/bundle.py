# -*- coding: utf-8 -*-
"""
    :copyright: © 2010-2019 by Farhan Ahmed.
    :license: BSD, see LICENSE for more details.
"""

NOT_FOUND = -1001
INVALID_REQUEST = -1002

MESSAGES = {
    NOT_FOUND: 'The bundle `{}` was not found.',
    INVALID_REQUEST: 'The provided bundle is not in the valid format.',
}
